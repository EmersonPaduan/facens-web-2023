
// simulando que recebemos estes dados de um BD ou API
let initial_data = [
    {id: 1, desc: 'Tarefa 1'},
    {id: 2, desc: 'Tarefa 2'},
    {id: 3, desc: 'Tarefa 3'},
    {id: 4, desc: 'Tarefa 4'}
];

let tasks = document.getElementById('tasks');
let tasks_done = document.getElementById('tasks-done');

initial_data.forEach(
    (task) => {
        let line = document.createElement('li');
        let label = document.createElement('label')
        line.appendChild(label);

        let checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.id = `task-${task.id}`
        checkbox.name = `task-${task.id}`;

        label.appendChild(checkbox);
        label.appendChild(document.createTextNode(task.desc));

        label.addEventListener('click', () => {
            let task_desc = label.innerText;
            let item = document.createElement('li');
            item.innerText = task_desc;
            line.remove();
            tasks_done.appendChild(item);
        });

        tasks.appendChild(line);
    }
);
