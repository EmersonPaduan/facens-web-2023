import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CursoComponent } from './curso/curso.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CursoComponent
  ],
  exports:[CursoComponent]
})
export class CursoModule { }
