import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { CursoModule } from './curso/curso.module';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, CursoModule, FormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title:string = 'Meu segundo projeto';
  nome:string = '';
  entrada:string = '';

  getNome() {
    return this.nome;
  }

  salvar() {
    this.nome = 'Emerson';
    this.entrada = this.nome;
  }


}
