import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Curso } from './curso/curso.component';

@NgModule({
  declarations: [Curso],
  imports: [
    CommonModule
  ],
  exports:[Curso]
})
export class CursosModule { }
