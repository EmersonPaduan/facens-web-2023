// alert('Boa noite')

var btn = document.getElementById("myBtn");
let cont = 0;
// console.log(btn);
// console.log(btn.innerHTML);

/* 

btn.innerHTML = "Clicked" 

*/


function exibirMsg() {
    cont++;
    console.log('Botão clicado ' + cont + " vez(s)");
}

// definindo uma ação a partir de uma função 
btn.addEventListener('click', exibirMsg);

let elem = document.getElementById("elemento");

// definindo uma ação a partir de uma função anônima
elem.addEventListener('mouseover', function () {
    console.log("Mouse sobre o elemento");
});

// definir a ação a partir de uma arrow function
let elem2 = document.getElementById("elemento2");

elem2.addEventListener('mouseover', () => {
    elem2.style.backgroundColor=  'red'
    elem2.style.color = 'white'
});