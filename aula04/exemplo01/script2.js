let vetor = [
    "Audi",
    "BMW",
    "Land Rover",
    2020,
    2022,
    true,
    function() {
        console.log('Boa noite');
    }
];

console.log(vetor);
console.log(vetor[0]);
console.log(vetor[3]);
console.log(vetor[vetor.length-1]);
vetor[vetor.length-1]();

vetor.forEach( function (value, index) {
    console.log(index, value);
} );